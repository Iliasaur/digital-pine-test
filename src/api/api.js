const API_URI = "https://api.rawg.io/api/"
const API_KEY = "62ab6b23bee84d16852c10b37a96dc54"


//------------- Запрос к API
const apiGet = (url) => {
  return new Promise(function(succeed, fail) {
    var request = new XMLHttpRequest();
    var urlWithKey = url + (url.indexOf("?") >= 0 ? "&" : "?") + "key=" + API_KEY;
    request.open("GET", urlWithKey, true);
    request.addEventListener("load", function() {
      if (request.status < 400) {
        succeed(JSON.parse(request.response));
      } else {
        fail(new Error("Request failed: " + request.statusText));
      }
    });

    request.addEventListener("error", function() {
      fail(new Error("Network error"));
    });

    request.send();
  });
}



const api = {
	
	//------------- Get Games
	getGames(filter, order) {
    let uri = API_URI + "games"
      + "?pageSize=" + filter.pageSize
      + "&page=" + filter.page
      + (filter.platforms.length > 0 ? "&platforms=" + filter.platforms.join() : "")
      + (filter.keyword ? "&search=" + encodeURIComponent(filter.keyword) : "")
      + (order.key ? "&ordering=" + (order.isDesc ? "-" : "") + order.key : "");
		return apiGet(uri).then(
      response => response
    )
	},


  //------------- Get Game
  getGame(slug) {
    let uri = API_URI + "games/" + slug
    return apiGet(uri).then(
      response => response
    )
  },


  //------------- Get Game Screenshots
  getGameScreenshots(slug) {
    let uri = API_URI + "games/" + slug + "/screenshots"
    return apiGet(uri).then(
      response => response
    )
  },


  //------------- Get Platforms
  getPlatforms() {
    return apiGet(API_URI + "platforms").then(
      response => response
    )
  }
}

export default api