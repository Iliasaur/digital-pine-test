import React from 'react'
import {connect} from 'react-redux'
import styled from 'styled-components'

import {getGames} from '../../redux/games-reducer.js'

import device from '../../theme/device'
import Preloader from '../ui/Preloader'
import Orderer from '../ui/Orderer'
import GamePreview from '../GamePreview'


const Cover = styled.div`
	width: 100%;
	margin-top: 1rem;
`

const StyledGameList = styled.div`
  @media ${device.tablet} { 
    display: flex;
  	flex-wrap: wrap;
  	justify-content: space-between;
  }
`

const H1 = styled.div`
	font-size: 3rem;
	font-weight: bold;
	margin-bottom: 1rem;
`

const Subline = styled.div`
	display: flex;
	flex-wrap: wrap;
	justify-content: space-between;
	align-items: center;
`

const Subtitle = styled.p`
	font-size: 1.2rem;
`

const PreloaderCover = styled.div`
	margin-top: ${props => props.gamesCount > 0 ? "1rem" : "5vh"};
`


class GameList extends React.Component {


	//------------- Constructor
	constructor(props) {
		super(props)
		this.ref = React.createRef()
	}


	//------------- Did Mount
	componentDidMount = () => {
		this.props.getGames()
		window.addEventListener("scroll", this.handleScroll)
	}


	//------------- Will Unmount
	componentWillUnmount() {
		window.removeEventListener("scroll", this.handleScroll)
  }


  //------------- Handle Scroll
  handleScroll = () => {
  	if (this.props.isFetching) { return }

    let contentHeight = this.ref.current.offsetHeight
    let yOffset       = window.pageYOffset
    let windowHeight = window.innerHeight
   
    if (yOffset + windowHeight >= contentHeight) {
      this.props.getGames()
    }
  }


  //------------- Render
	render = (props) => {
		let hasPlatform = this.props.filter.platforms.length > 0
		let hasSinglePlatform = this.props.filter.platforms.length === 1

		let platformTitle = ""
		if (hasSinglePlatform) {
			let platform = this.props.platforms.find(platform => platform.id === this.props.filter.platforms[0])
			platformTitle = platform ? platform.name : "Unknown"
		}

		let listTitle = hasPlatform
			? (hasSinglePlatform ? "Platform: " + platformTitle : "Custom platforms")
			: "Fresh and Popular"

		let subtitle = this.props.filter.keyword
			? this.props.filter.keyword + (this.props.isFetching ? "" : ": " + (new Intl.NumberFormat().format(this.props.gamesCount)) + " " + (this.props.gamesCount === 1 ? "item" : "items"))
			: (hasPlatform ? "" : "Based on player counts and release date")
		
		return (
			<Cover className="gamelist">
				<H1 className="gamelist__title">{listTitle}</H1>
				<Subline className="gamelist__subline">
					{ subtitle && <Subtitle className="gamelist__subtitle">{subtitle}</Subtitle> }
					<Orderer />
				</Subline>
				<StyledGameList className="gamelist__body" ref={this.ref}>
					{this.props.games.map(game => 
						<GamePreview
							key={game.id}
							game={game}
						/>
					)}
				</StyledGameList>
				{ this.props.isFetching && <PreloaderCover className="preloader__cover" gamesCount={this.props.gamesCount}><Preloader /></PreloaderCover> }
			</Cover>
		)
	}

}



/*****************************************************************************

					CONTAINER

*****************************************************************************/

let mapStateToProps = (state) => {
	return {
		gamesCount: state.games.count,
		games: state.games.list,
		filter: state.games.filter,
		isFetching: state.games.isFetching,
		platforms: state.platforms.list
	}
}

export default connect(mapStateToProps, {
	getGames
})(GameList)
