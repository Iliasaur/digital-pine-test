import React from 'react'
import styled from 'styled-components'
import Hamburger from '../ui/Hamburger'
import device from '../../theme/device'


const Circle = styled.div`
	position: fixed;
	bottom: 1rem;
	right: 1rem;
	display: flex;
	justify-content: center;
	align-items: center;
	background-color: ${(props) => props.theme.color.black};
	border-radius: ${(props) => props.theme.borderRadius.huge};
	padding: 1rem;

	@media ${device.laptop} { 
    display: none;
  }
`

const Footer = (props) => {

	return (
		<Circle className="footer">
			<Hamburger handleClick={props.toggleMenuVisibility} />
		</Circle>
	)

}

export default Footer