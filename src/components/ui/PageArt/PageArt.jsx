import styled from 'styled-components'


const Art = styled.div`
 	position: absolute;
  top: 0;
  left: 0;
  z-index: -1;
  width: 100%;
  height: 100%;
`

const Wrapper = styled.div`
 	height: 500px;
`

const Background = styled.div`
	height: 500px;
	background-color: transparent;
	background-size: cover;
	background-image: linear-gradient(rgba(15, 15, 15, 0), rgb(21, 21, 21)), linear-gradient(rgba(21, 21, 21, 0.8), rgba(21, 21, 21, 0.5)), url(${props => props.bg});
	z-index: 1;
`


const PageArt = (props) => {
	return (
		<Art className="pageart">
			<Wrapper className="pageart__wrapper">
				<Background bg={props.bg} className="pageart__bg" />
			</Wrapper>
		</Art>
	)
}

export default PageArt