import React from 'react'
import styled from 'styled-components'


const Trigger = styled.div`
	position: absolute;
	top: 0;
	width: 50px;
	height: 100%;
	background-color: transparent;
	cursor: pointer;
	transition: background-color 0.25s;
	background-position: center;
	background-repeat: no-repeat;

	&.slider__trigger_left {left: 0;}
	&.slider__trigger_right {right: 0;}
`

const Cover = styled.div`
	position: relative;
	width: 100%;
	box-sizing: border-box;
	display: flex;
	align-items: center;
	overflow: hidden;

	&:hover ${Trigger} {
		background-color: rgba(128,128,128,0.2);
		&.slider__trigger_left {
			background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAMOSURBVHja7Jq9ixNBGMZnN4mugnKFhefZ2ChopaJoIWihFqL4UXlYCBYWdn7gf2DhgZWNh1gofoMi4gdoISr4hdgJVnIHahEIksaQmKzPe05wOcy8k92d2dnNvvCrdm65J8M888y744VhKEapfDFiVQouBZeCR1RwvV4XzWZTtFot0W63Ra/XE+T4MVx/D5gB4Ty+g71pC/bibkuNRkMEQSCq1aqoVCrC9//9dp7n6b5mE3gJggHPSfREmoKrcf+wVqvNiSWhfYFDCKUaB/cVYo1UbMEklgTS7MYoEnlPY/bOOLOGaWZJLC0JEj7k7F4EW5gx58GN1Ke4bzTD0ul0RLfbnSNqWBqcDvl6Cipx/zcVsU2LhEbXr2btBE+Aah18kbP/09l9WPNHWw3uMGKb4KApsYnX8BDuvEQ68phiTA8cBp+dTVqaM+tL81mr4ciPje9LSQxA06ymNEzqqgmDStW0NOsIuMaM+Qi2gV82gkciwf09eEBtAK/AYsUrfsh4+c35wwMjdjl4wIhtgf02xSYSrBAbSEdeybziBHifq/PwgOWgGxuvZHIgTtkFM42Ntl0689ho3KXnxcZ3TJKi2LjVdJKykaWdiY02BLsVGy2YllOx0bRpTYLrzJi3YIcMGU5UEsGUkFYwzzfLzqMzlWQNhymNyY1grqM4ITuTQVEE35QRUVUUMqadmuKErueDhxpOfaoILh0NHm/AOiZ47AOP8uzS0Volj3rLRiFaUn2V+/JvxZil0sTGiiCY6hk4y4xZA24zJ6rcCKa6AC4zY3aBc3lfw9FaAJ6Lv51IVR3Louthqk1LTbwPTF+L8vV2eY7OvWCq9eC1KEqbVqM+gaNMnqZbANTOXVQEwVR3wRQzZiO4lJdomWb8PFmEb0vOxU/P4m1aJ+KnzZt4TsRP21cPdePnLVPxM4u7ljrxc7ep+OlldCN+IXgh+I9uk7Kzkr1g5vuwTo3L+Km6jZf6XUsT34d1i2LlAWG5Z531fWma4UNg9j/PKF8fL8oaLkTHoxRcCi4Fl4KN1x8BBgDy9z7G2m9YnwAAAABJRU5ErkJggg==");
		}
		&.slider__trigger_right {
			background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAMESURBVHja7Jq9axRBGMZn9+7gIijhKs1ZK9iKX6BiIUoIgh+ViiA2FnZq8D+wMGBlo2gKETURDFEQwULBQlHQSgsbUTQKlyKcIOd9rc97zIIe2Zm5vZ3Zmb194ccFdrjMczPzzjPvrBcEARul8NmIRS44F5wLzgX3xyGwBII+voCpQb6IdhCi2+2yZrPJGo0Gq9frrFarxe6cp2Fb+g4mIp41wF7wVlVwGCS60+mwdrvdE16pVJyY0mWwADYojYbn9Qj/9n2fFYtFViqVrJrS05LnVfCQi1efihBcKBR6nyTaJsF3wRVJm53g2iBiCZriJJpGOnaEiSFhCuBpII+Lqt8ZrmGi1WrF7pun0UuPg9dgs6BNB0yCZ4MMEImnkbYtaa2Ao6AuaEO9ngebBsnYNhuPj+A47SqSmUCZe61sHf+brW12Wk8UMvcWnux87SOtKWmtxm2FJDajkryG6YdnsAAwBl6CrZJ2p8AdXZ3wDFc8qtxWipzWb7AHvIuakeF6dsFaks8+zD11VKwBi2B9VPIaZpDSOB6+AeckbTbyzF0WZWyXzsOzce2nS1lau/20LUsbs582Zeko0/EKrJPY1B3gUxZqWonZT5eKeInaT1uTljb7aXPSWq3m9ZxvSaI4yUfbyaTVHxPcnFQFbZYkz61fw/+tsoTaOCG4zCuastGbzorgGwrrlyzpvSxk6QsKGfox8LOQpenO6ZFkxn0Au8CvUbCWy2A7+Oy60xrnSUoktg1OJCE2bcFUk56TnJQoLiV1Ukpb8GVwQNLmJrjqchEvjDPglqQNVTj3g6brgulc+4KJr0u/gW3gZ9L/3NYy7W7wXkcHTK5hKsQvSsTSr39al1jTgq8z+a3DDHig92hixjaeN2EbbbGWxmyjDUnLqG1Mew0bt41pCibbeN+0bUxTMNnGg6ZtY1prmIrqsooiXa/sA3+yIFj0rmX4nGzjj2G20rhXpqZPS3QRfmQYsb1Rsux++Cwfxf74Co4xxTdpXd2HrYv8jfhccC44F+xU/BVgALGHTsj/dTi3AAAAAElFTkSuQmCC");
		}
	}
`

const Slide = styled.div`
	position: absolute;
	width: 100%;
	height: auto;

	&.slider__image_prev {
		transform: translateX(-100%);
		z-index: 1;
	}

	&.slider__image_next {
		transform: translateX(100%);
	}
`

const Image = styled.img`
	width: 100%;
	height: auto;
`



class Slider extends React.Component {

	state = {
		index: 0
	}

	timer = false
	prevTimer = false
	nextTimer = false
	
	//------------- Constructor
	constructor(props) {
		super(props)
		this.ref = React.createRef()
		this.prevRef = React.createRef()
		this.nextRef = React.createRef()
	}


	//------------- Did Mount
	componentDidMount() {

		let ratio = (this.props.srcs[0].height > 0 && this.props.srcs[0].width > 0) ? this.props.srcs[0].height / this.props.srcs[0].width : 0.75
		if (ratio > 0) {
			this.ref.current.style.height = Math.floor(this.ref.current.offsetWidth * ratio) + "px"
		}

		if (this.props.autoplay) {
			let delay = isNaN(this.props.delay) || Number(this.props.delay) < 1000 ? 1000 : Number(this.props.delay)
			this.timer = window.setInterval(this.next, delay)
		}
	}


	//------------- Will Unmount
	componentWillUnmount() {
		if (this.timer) { window.clearInterval(this.timer) }
		if (this.prevTimer) { window.clearTimeout(this.prevTimer) }
		if (this.nextTimer) { window.clearTimeout(this.nextTimer) }
	}


	//------------- Prev
	prev = () => {
		this.prevRef.current.style.transform = "translateX(0)"
		this.prevRef.current.style.transition = "all 0.25s"
		this.prevTimer = window.setTimeout(() => {
			if (this.prevTimer) { window.clearTimeout(this.prevTimer) }
			this.setState({...this.state, index: this.state.index > 0 ? this.state.index - 1 : this.props.srcs.length - 1})
			this.prevRef.current.style.transition = ""
			this.prevRef.current.style.transform = "translateX(-100%)"
		}, 250)
	}


	//------------- Next
	next = () => {
		this.nextRef.current.style.transform = "translateX(0)"
		this.nextRef.current.style.transition = "all 0.25s"
		this.nextTimer = window.setTimeout(() => {
			if (this.nextTimer) { window.clearTimeout(this.nextTimer) }
			this.setState({...this.state, index: this.state.index >= this.props.srcs.length - 1 ? 0 : this.state.index + 1})
			this.nextRef.current.style.transition = ""
			this.nextRef.current.style.transform = "translateX(100%)"
		}, 250)
	}


	//------------- Render
	render = () => {
		const prevImageIndex = this.state.index ? this.state.index - 1 : this.props.srcs.length - 1
		const nextImageIndex = this.state.index < this.props.srcs.length - 1 ? this.state.index + 1 : 0

		return (
			<Cover className="slider" ref={this.ref}>
				<Slide className="slider__image slider__image_prev" key="0" ref={this.prevRef}>
					<Image src={this.props.srcs[prevImageIndex].image} alt="" />
				</Slide>
				<Slide className="slider__image slider__image_actual" key="1">
					<Image src={this.props.srcs[this.state.index].image} alt="" />
				</Slide>
				<Slide className="slider__image slider__image_next" key="2" ref={this.nextRef}>
					<Image src={this.props.srcs[nextImageIndex].image} alt="" />
				</Slide>
				<Trigger className="slider__trigger slider__trigger_left" onClick={this.prev} />
				<Trigger className="slider__trigger slider__trigger_right" onClick={this.next} />
			</Cover>
		)
	}
}

export default Slider