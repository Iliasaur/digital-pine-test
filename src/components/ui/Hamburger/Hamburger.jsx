import React from 'react'
import styled from 'styled-components'


const Cover = styled.div`
  position: relative;
`

const Label = styled.label`
  display: flex;
  align-items: center;
  width: 26px;
  height: 26px;
  cursor: pointer;
  z-index: 1;

  & > span,
	& > span::before,
	& > span::after {
	  display: block;
	  position: absolute;
	  width: 100%;
	  height: 3px;
	  background-color: ${(props) => props.theme.color.white};
	  transition: background-color 0.25s;
	}
	& > span::before {
	  content: '';
	  top: -8px;
	}
	& > span::after {
	  content: '';
	  top: 8px;
	}

	&:hover > span, &:hover > span::before, &:hover > span::after {
		background-color: ${(props) => props.theme.color.gray};
	}
`


const Hamburger = (props) => {

	return (
		<Cover className="hamburger">
		  <Label className="hamburger__label" onClick={ props.handleClick }>
		    <span></span>
		  </Label>
		</Cover>
	)

}

export default Hamburger