import React from 'react'
import styled from 'styled-components'



const Icon = styled.div`
	width: 21px;
	height: 21px;
	margin-right: 8px;
	border-radius: ${props => props.theme.borderRadius.large};
	background-repeat: no-repeat;
	background-position: center center;
	background-image: url(${props => props.isExpanded 
		? "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAAJCAYAAADpeqZqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAABFSURBVHjajMvBDQAgDALAyko6ZIezO+lDXxYsCeFBrrm7kfTdyA4IMM+W0AXGIAQYDEKAYBACGIMQwBjEB6QQBfDAJcAA9UUY2zf7FMcAAAAASUVORK5CYII="
		: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAAJCAYAAADpeqZqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAABeSURBVHjahNEBCsAgCAXQv3+ldkgPV2dqCTbaMg2ECh+ZXiLSAdwjGvJVRlTaodpFCnRDeyWDL9B8WlkR/ADNn+Wd4AZmeQjgBv7Ig/A6S+fTK3RHwUO3WjS7R4ABANg4G3isZBlgAAAAAElFTkSuQmCC"
	});
	transition: background-color 0.25s;
`


const Cover = styled.div`
	display: flex;
	align-items: center;
	color: ${(props) => props.theme.color.gray};
	cursor: pointer;

	&:hover {
		${Icon} {
			background-color: ${props => props.theme.color.gray};
			background-image: url(${props => props.isExpanded 
				? "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAAJCAYAAADpeqZqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAA5SURBVHjaYmDADYwZSAQgDf9J0QjT8J9YjcZoiglqxKYIr0Z8kljliHEGhhpiPYyskaSgBWsECDAAIEMer7FLzXIAAAAASUVORK5CYII="
				: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAAJCAYAAADpeqZqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAABASURBVHjaYmBgYPgPxMYMxAFjqHowQYxGYyS1KBxjIjQY4xUkQg6rJDGuwFBErH9RFJMSsnCNRGtA1ogVAAQYALTrHq/YBTdEAAAAAElFTkSuQmCC"
			});
		}
	}
`


const Expander = (props) => {
	
	return (
		<Cover className="expander" onClick={props.onClick} isExpanded={props.isExpanded}>
    	<Icon className="expander__icon" isExpanded={props.isExpanded} />
    	<div className="expander__label">{props.isExpanded ? "Hide" : "Show all"}</div>
  	</Cover>
	)

}


export default Expander