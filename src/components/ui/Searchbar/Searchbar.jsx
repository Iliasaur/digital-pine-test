import React from 'react'
import styled from 'styled-components'


const Cover = styled.div`
	position: relative;
	display: flex;
	justify-content: space-between;
	padding: 0.5rem;
	border-radius: ${props => props.theme.borderRadius.huge};
	background-color: ${props => props.theme.color.darkGray};
`

const Tools = styled.div`
	display: flex;
`


const Button = styled.div`
	width: 32px;
	height: 32px;
	background-position: center;
	background-repeat: no-repeat;
	cursor: pointer;
`

const ButtonLens = styled(Button)`
	background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIFSURBVHjarJXLK0RRHMdnrmFCnimPhceCjYUiI2SyEGGBpYWdZxY2iPwDNsrCKxvNUPwBI9lJitVshLBgIZKUPBYezfU99b05rnOve5lffboz9/zO+Z7fub/f73h1XfcoLBd0giAoAV7wBKIgAvY8Tk0ISCSBCXCnf9k1uAQP0rstUGmaq0T+kwYiXOAMDIFS4Od4DmgEq/R5Bh1OBfzclbAFkP7LxHZG+QGanAhMcvFFJ2GTKkZxAbLsBPLAPTgFKS4EBKPc2LidwACd+l0uLkgFN+Ac+KwENsArKGYWtbkUWeEGy1TjGjK1CNyDK1AOejzu7JDPQtWgxucb+AAZoBnkuxB4sRvU6JAFUsElyAbDLgSK+HyyquRpnmEDz+2A+d3q8Bvsg0eQYfWR6ymwzpdB8M4PPyRVsooA567ZpakGtunYwoEu9iCjbcyCPlAgTU4Au4y25rdKrgAvLP+A1HtEbcxToFtqISKdl7mBOavFY7HYt2YnGtcbj2bMpqprwY7UWffZavpUxeY13QciRZd4B9yCTXDETCvm/VBH31n+Dkjzw6AXvFvdB4JMRnCs/zRxJ4QZhfCdUviEjEjMR2QmkeXfRKp5Z8g+g7raQljcZ75w/kIyd6wyEWnifwWMSMMqBUQxEw8BD89cJXISLwGj8MwiI/EUMCKZAVGxuMiiTwEGADvs388dPEUIAAAAAElFTkSuQmCC");

	&:hover {
		background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAI6SURBVHjarNVPSBRhGMfx2XWoQ2QUQdah3Y0IIugQISmYLGSZBnWKLgmmEdlNQwU9ePNggiLpQoeo7VSU0R8Iu0QErpBdQhC9dAgqTMpMrTbL7ws/YZJ5pxncFz7MjDvj8877PO8zsVwu5/iMHTiDo0ghhnm8wRO8ckIOd831BjSjBdv1tw/4hf2oQhueoUMBA0fcc74ZD9CNL2jCPr1BEnuRxh1U4yVOhw2wEXdRiyEc1nEaP3XPZ7zAeZzCEu7jWJgAzZpVRjP/9p+JPdX9P3ADW4MClOAqprT2Ycc4urR8F4MCmHXchl4sOtGGWcaPCuDaAqRVJSOqopoIARZUUaYA9tgCJDCL9zigJEYZb3XcHZRk8wa/sQXHsTPiWwRW0YKqYBPeKR9XIgRI6DhvC/AaxTikAGNox8mQAdL655O2AI913qRjK/7iIS5rE9pGKY7gEeZsAUZVQedwQi3grBI/qCT2qRR3eZ4twjUsYyAoB38060X1GTOrYRzEJTzXvd9ltSmaPVCh3T9mCxDztGuz4e5peTpx3bLxytQQK3Wd0xKZXnVT1egbwFGJZtRBP6nnTKjSkvo+lOvePp2Xep6/jUbkbd+DEVWTWe96XFjzu0lkVsszqm+CN0Cdjg2rb+LXP76iRzNMeXbonErRW++zPs//E8QNKMG8OuxUwD23lJM6nyDmM9vgOusbS1rzmE8PM9czcWf9I69cZX1+qylEAEcJrfcJknGdwo1lvcmM+pPJT/+KAAMATOqH77HXglIAAAAASUVORK5CYII=");
	}
`

const ButtonClear = styled(Button)`
	margin-right: 1rem;
	background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAOCAYAAAAfSC3RAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAABkSURBVHjaYvgPAfpAzEAkBqlFMIDAmAhNxlC11ugCAXg0BSBbgE0iBYumFHSDcSmoRRKrxWYgNifBFMZAMbpBODUyIGmAGYChhomBXEAtp5IVOGRFB1kJQI2MJKfPQG4iBwgwACZ9SKh9kuyZAAAAAElFTkSuQmCC");

	&:hover {
		background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAOCAYAAAAfSC3RAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAADfSURBVHjanJIxDoJAEEXXDZewpLGVBEsPAQegg2NgoceQTnu4hAmFm0BpZUnjHfSv+ZtMNkDQSV7CzsxnZnZn1bbtWykVgV4tsy3oNEUdiBeIYubuNSvtwB0kM6KEOTb3puk0IAU1yEdEOWMpc5UWwQYU4AxK4S/pK5jztcD7cwXW4Aie9NnvA2NqSmjtRNGF5wxc/SSt/rSxiiXby3i2lUN2MinMxUyuvZC+Qc4ZeO/kbq/yZh4Ye7mbDcRGuHdqRtqvKKq5AMYKN2IjzMx9NGLDIit8/LDkxu32R4ABAGa/NJIeYsC/AAAAAElFTkSuQmCC");
	}
`


const Input = styled.input`
	border: 0;
	font-size: 1.2rem;
	background-color: transparent;
	outline: none;
	color: ${props => props.theme.color.white};
	width: ${props => props.isEmpty ? "calc(100% - 33px)" : "calc(100% - 65px - 1rem)"};
`


class Searchbar extends React.Component {

	state = {
		keyword: ""
	}


	//------------- Constructor
	constructor(props) {
		super(props)
		this.ref = React.createRef()
	}

	
	//------------- Did Mount
	componentDidMount() {
		this.setState({...this.state, keyword: this.props.keyword})
	}


	//------------- Did Update
	componentDidUpdate(prevProps) {
		if (prevProps.keyword !== this.props.keyword) {
			this.setState({...this.state, keyword: this.props.keyword})
		}
	}


	//------------- On Change
	onChange = (e) => {
		this.setState({...this.state, keyword: e.target.value})
	}


	//------------- On Key Press
	onKeyPress = (e) => {
		if (e.charCode === 13) {
			this.submit()
		}
	}


	//------------- Clear
	clear = () => {
		this.setState({...this.state, keyword: ""})
		this.props.onChange("")
		this.ref.current.focus()
	}


	//------------- Submit
	submit = () => {
		this.props.onChange(this.state.keyword)
		this.ref.current.focus()
	}


	//------------- Render
	render = () => {
		return (
			<Cover className="searchbar">
				<Input
				 	className="searchbar__input"
					type="text"
					placeholder={this.props.placeholder}
					value={this.state.keyword}
					onChange={this.onChange}
					onKeyPress={this.onKeyPress}
					isEmpty={this.state.keyword ? false : true}
					maxLength={this.props.maxLength > 0 ? this.props.maxLength : undefined}
					ref={this.ref}
				/>
				<Tools className="searchbar__tools">
					{this.state.keyword && <ButtonClear className="searchbar__button_clear" onClick={this.clear} /> }
					<ButtonLens className="searchbar__button_submit" onClick={this.submit} />
				</Tools>
			</Cover>
		)
	}
	
}


export default Searchbar