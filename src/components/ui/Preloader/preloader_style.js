import styled from 'styled-components'

export const Preloader = styled.div`
	display: flex;
	flex-direction: column;
  justify-content: center;
  align-items: center;
`

export const Body = styled.div`
	padding: 1rem;
	background-color: ${(props) => props.theme.color.dark};
	border-radius: ${(props) => props.theme.borderRadius.medium};
`

export const Circonf = styled.div`
	display: block;
  width: 48px;
  height: 48px;
  margin: 0 auto;
  border-radius: 50%;
  border: 3px solid lime;
  animation: circ-anim 0.9s linear infinite;
  border-color: ${(props) => props.theme.color.blue};
  border-bottom-color: transparent;

  @keyframes circ-anim {
	  from {transform: rotate(0);}
	  to {transform: rotate(360deg);}
	}
`

export const CirconfMicro = styled(Circonf)`
	width: 24px;
	height: 24px;
	animation-duration: 0.7s;
	border-width: 1.5px;
`

export const CirconfSmall = styled(Circonf)`
	width: 32px;
	height: 32px;
	animation-duration: 0.8s;
	border-width: 2px;
`

export const CirconfLarge = styled(Circonf)`
	width: 64px;
	height: 64px;
	animation-duration: 1s;
	border-width: 4px;
`


export const Title = styled.div`
	color: ${(props) => props.theme.color.white};
	margin-top: 1.5rem;
	font-size: 1.2rem;
	text-align: center;
`