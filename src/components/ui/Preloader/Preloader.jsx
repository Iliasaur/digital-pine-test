import React from 'react'
import * as ps from './preloader_style'


const Preloader = (props) => {

	const renderSwitch = () => {
	  switch(props.size) {
	    case "micro": return <ps.CirconfMicro />
	    case "small": return <ps.CirconfSmall />
	    case "large": return <ps.CirconfLarge />
	    default: return <ps.Circonf />
	  }
	}

	return (
		<ps.Preloader className="preloader">
			<ps.Body className="preloader__body">
				{ renderSwitch() }
			</ps.Body>

			<ps.Title className="preloader__title"></ps.Title>
		</ps.Preloader>
	)

}

export default Preloader