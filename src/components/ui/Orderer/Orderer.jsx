import React from 'react'
import {connect} from 'react-redux'
import styled from 'styled-components'

import {setOrderKey, toggleOrderDirection} from '../../../redux/games-reducer.js'
import gameOrder from '../../../data/game_order'


const Cover = styled.div`
	display: flex;
	align-items: center;
`

const Label = styled.label`
	
`

const Order = styled.div`
	margin: 0 0 0 0.5rem;
	padding: 0.3rem 0.7rem;
	cursor: pointer;
	border-radius: ${props => props.theme.borderRadius.large};
	background-color: ${props => props.isSelected ? props.theme.color.darkGray : "transparent"};
	transition: background-color 0.25s;

	&:hover {background-color: ${props => props.theme.color.darkGray};
`

const Director = styled.div`
	width: 1.5rem;
	height: 1.5rem;
	margin-left: 0.5rem;
	background-position: center;
	background-repeat: no-repeat;
	cursor: pointer;
	border-radius: ${props => props.theme.borderRadius.large};
	background-image: url(${props => props.isDesc 
		? "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAAJCAYAAADpeqZqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAABeSURBVHjahNEBCsAgCAXQv3+ldkgPV2dqCTbaMg2ECh+ZXiLSAdwjGvJVRlTaodpFCnRDeyWDL9B8WlkR/ADNn+Wd4AZmeQjgBv7Ig/A6S+fTK3RHwUO3WjS7R4ABANg4G3isZBlgAAAAAElFTkSuQmCC"
		: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAAJCAYAAADpeqZqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAABFSURBVHjajMvBDQAgDALAyko6ZIezO+lDXxYsCeFBrrm7kfTdyA4IMM+W0AXGIAQYDEKAYBACGIMQwBjEB6QQBfDAJcAA9UUY2zf7FMcAAAAASUVORK5CYII="
	});

	&:hover {background-color: ${props => props.theme.color.darkGray};
`



class Orderer extends React.Component {

	//------------- Handle Order Key
	handleOrderKey = (e) => {
		this.props.setOrderKey(e.target.dataset.key)
	}


	//------------- Handle Order Direction
	handleOrderDirection = () => {
		this.props.toggleOrderDirection()
	}


	//------------- Render
	render = () => {
		return (
			<Cover className="orderer">
				<Label className="orderer__label">Order by:</Label>
				{ gameOrder.map(
					order => <Order
						className="orderer__item"
						isSelected={(this.props.order.key === order.key)}
						data-key={order.key}
						key={order.key}
						onClick={this.handleOrderKey}
					>{order.title}</Order>
				) }
				<Director
					className="orderer__director"
					isDesc={this.props.order.isDesc}
					onClick={this.handleOrderDirection}
				/>
			</Cover>
		)
	}
	
}

/*****************************************************************************

					CONTAINER

*****************************************************************************/

let mapStateToProps = (state) => {
	return {
		order: state.games.order
	}
}

export default connect(mapStateToProps, {
	setOrderKey,
	toggleOrderDirection
})(Orderer)