import React from 'react'
import {Route} from 'react-router-dom'
import {connect} from 'react-redux'
import {NavLink} from 'react-router-dom'
import {clearGame} from '../../redux/game-reducer'
import {setPlatform} from '../../redux/games-reducer.js'

import * as mc from './MenuComponents'
import MenuPlatforms from './MenuPlatforms'
import IconClose from '../ui/IconClose'






class Menu extends React.Component {

	//------------- Reset To All Games
	resetToAllGames = () => {
		this.props.clearGame()
		this.props.setPlatform(null)
	}


	//------------- Render
	render = () => {
		return (
			<>
				{ this.props.isMenuVisible && <mc.MenuMask className="menu__mask" onClick={ this.props.toggleMenuVisibility } /> }

				<mc.MenuBoxHolder className={"menu__holder" + (this.props.isMenuVisible ? " visible" : "")} isScrolled={this.props.isScrolled}>
					<mc.MenuBox className="menu">
			    	<mc.MenuIconCover as="li" className="menu__icon_cover">
			    		<mc.MenuIcon  className="menu__icon" onClick={ this.props.toggleMenuVisibility }>
			    			<IconClose />
			    		</mc.MenuIcon>
			    	</mc.MenuIconCover>
				  
				  	<mc.MenuItemClickable as="li" className="menu__item"><NavLink to="/">Home</NavLink></mc.MenuItemClickable>
				  	
				  	<Route render={({history}) => (
				  		<mc.MenuItemClickable
				  			as="li"
				  			className="menu__item"
				  			onClick={() => {
				  				this.resetToAllGames()
				  				history.push("/")
				  			}}>All Games</mc.MenuItemClickable>
				  	)} />

				    <li>
				    	<mc.MenuItem as="div"  className="menu__item">Platforms</mc.MenuItem>
				    	<MenuPlatforms />
				    </li>
				  </mc.MenuBox>
				 </mc.MenuBoxHolder>
			</>
		)
	}

}



/*****************************************************************************

					CONTAINER

*****************************************************************************/

let mapStateToProps = () => ({})


export default connect(mapStateToProps, {
	clearGame,
	setPlatform
})(Menu)
