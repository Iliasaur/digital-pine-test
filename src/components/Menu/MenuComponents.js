import styled from 'styled-components'
import device from '../../theme/device'


export const MenuMask = styled.div`
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  margin: 0;
  padding: 0;
  background-color: ${props => props.theme.color.black};
  opacity: 0.5;
  z-index: 998;
`

export const MenuBoxHolder = styled.div`
	display: flex;
	flex-direction: column;
	position: fixed;
  visibility: hidden;
  max-width: calc(100vw - 2rem);
	max-height: calc(100vh - 2rem);
	top: 1rem;
  right: 1rem;
  margin: 0;
  padding: 0;
  background-color: ${props => props.theme.color.dark};
  border-radius: ${props => props.theme.borderRadius.large};
  z-index: 999;

  &.visible {visibility: visible;}

  @media ${device.laptop} { 
    visibility: visible;
    border-radius: 0;
    position: ${props => props.isScrolled ? "fixed" : "relative"};
    left: ${props => props.isScrolled ? "1rem" : "0"};
    background-color: transparent;
    width: auto;
    z-index: 0;
    max-width: calc(250px - 1rem);
    max-height: calc(100vh - ${props => props.isScrolled ? "40px" : "120px"});
  }
`

export const MenuBox = styled.ul`
  list-style: none;
  text-align: right;
  overflow-y: scroll;
  margin 1rem 0;
  padding: 0;

  &::-webkit-scrollbar {height: 8px; width: 8px; background: transparent; overflow: visible;}
  &:hover ::-webkit-scrollbar {display: block;}
  &::-webkit-scrollbar-corner {display: none;}
  &::-webkit-scrollbar-button {display: none; height:0; width: 0;}
  &::-webkit-scrollbar-button:start:decrement,::-webkit-scrollbar-button:end:increment {display: block;}
  &::-webkit-scrollbar-button:vertical:start:increment,::-webkit-scrollbar-button:vertical:end:decrement {display: none;}
  &::-webkit-scrollbar-track {-moz-background-clip: border; -webkit-background-clip: border; background-clip: padding-box; background-color: transparent};}
  &::-webkit-scrollbar-track:vertical, ::-webkit-scrollbar-track:horizontal {border-left-width: 0; border-right-width: 0;}
  &::-webkit-scrollbar-track:vertical,::-webkit-scrollbar-track:horizontal,::-webkit-scrollbar-thumb:vertical,::-webkit-scrollbar-thumb:horizontal,::-webkit-scrollbar-track:vertical,::-webkit-scrollbar-track:horizontal,::-webkit-scrollbar-thumb:vertical,::-webkit-scrollbar-thumb:horizontal {border-style: solid; border-color: transparent;}
  &::-webkit-scrollbar-thumb {-webkit-box-shadow: inset 1px 1px 0 rgba(0,0,0,.1),inset 0 -1px 0 rgba(0,0,0,.1); background-clip: padding-box; background-color: ${(props) => props.theme.color.darkGray}; min-height: 28px;}
  &::-webkit-scrollbar-thumb:hover {background-color: transparent;}
  &::-webkit-scrollbar-thumb:active {background-color: transparent;}
  &::-webkit-scrollbar-thumb:vertical, ::-webkit-scrollbar-thumb:horizontal {border-width: 0; border-left-width: 0; border-right-width: 0;}

  @media ${device.laptop} {
    text-align: left;
    margin 0;
  }
`


export const MenuItem = styled.a`
  display: block;
  padding: 1rem;
  color: ${(props) => props.theme.color.white};
  text-decoration: none;
  font-size: 1.8rem;
  font-weight: bold;
  transition: color 0.25s, background-color 0.25s;
`

export const MenuItemClickable = styled(MenuItem)`
  cursor: pointer;
  
  &:hover {
    color: ${props => props.theme.color.gray};
  }
`


export const MenuIconCover = styled.div`
	display: flex;
	justify-content: flex-end;

  @media ${device.laptop} {
    display: none;
  }
`

export const MenuIcon = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	padding: 0.8rem;
	cursor: pointer;
	margin-top: 0.5rem;
	margin-right: 0.5rem;

	&:hover {
		border-radius: ${props => props.theme.borderRadius.huge};
		background-color: ${props => props.theme.color.gray};
	}
`