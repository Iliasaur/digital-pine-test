import React from 'react'
import {Route} from 'react-router-dom'

import styled from 'styled-components'
import {getPlatforms} from '../../../redux/platforms-reducer.js'

import {setPlatform} from '../../../redux/games-reducer.js'
import {connect} from 'react-redux'
import device from '../../../theme/device'
import Expander from '../../ui/Expander'
import Preloader from '../../ui/Preloader'
import mainPlatforms from '../../../data/main_platforms'


const Cover = styled.div`
	display: flex;
	flex-direction: column;
	align-items: ${props => props.isFetching ? "center" : "flex-end"};

	@media ${device.laptop} {
    align-items: flex-start;
  }
`

const PlatformsList = styled.div`
	display: flex;
	flex-wrap: wrap;
	justify-content: flex-end;
	padding: 0 0 1rem 0;

	@media ${device.laptop} {
		display: block;
		font-size: 3rem;
    justify-content: flex-start;
  }
`


const Platform = styled.div`
	margin: 0.7rem 1rem;
	font-size: 1.2rem;
	cursor: pointer;
	transition: color 0.25s;
	
	&.selected {
		color: ${props => props.theme.color.blue};
	}

	&:hover {
		color: ${props => props.theme.color.gray};
	}

	@media ${device.laptop} {
		margin-left: 2rem;
  }
`


const PaddedExpander = styled.div`
	margin-right: 1rem;
	font-size: 1.2rem;

	@media ${device.laptop} {
    margin-right: 0;
    margin-left: 1rem;
  }
`


class MenuPlatforms extends React.Component {

	state = {
		isExpanded: false
	}

	//------------- Did Mount
	componentDidMount = () => {
		this.props.getPlatforms()
	}


	//------------- Toggle Expander
	toggleExpander = () => {
		this.setState({...this.state, isExpanded: !this.state.isExpanded})
	}


	//------------- Click Platform
	clickPlatform = (e) => {
		this.props.setPlatform(Number(e.target.dataset.platformId))
	}


	//------------- Render
	render() {
		return (
			<Cover className="menu_platforms" isFetching={this.props.isFetching}>
				{this.props.isFetching && <Preloader size="small" />}

				{!this.props.isFetching &&
					<>
						<Route render={({history}) => (
							<PlatformsList className="menu_platforms__list">
								{this.props.platforms.filter(platform => this.state.isExpanded ? true : (mainPlatforms.indexOf(platform.slug) >= 0)).map(
									platform => <Platform
										key={platform.id}
										data-platform-id={platform.id}
										onClick={(e) => {
											this.clickPlatform(e)
											history.push("/")
										}}
										className={"menu_platforms__item" + (this.props.selectedPlatforms.indexOf(platform.id) >= 0 ? " selected" : "")}
										isSelected={this.props.selectedPlatforms.indexOf(platform.id) >= 0}
									>{platform.name}</Platform>
								)}
							</PlatformsList>
						)} />
						<PaddedExpander className="menu_platforms__expander">
							<Expander isExpanded={this.state.isExpanded} onClick={this.toggleExpander} />
						</PaddedExpander>
					</>
				}
			</Cover>
		)
	}
	
}




/*****************************************************************************

					CONTAINER

*****************************************************************************/

let mapStateToProps = (state) => {
	return {
		gameSlug: state.game.slug,
		platforms: state.platforms.list,
		isFetching: state.platforms.isFetching,
		selectedPlatforms: state.games.filter.platforms
	}
}

export default connect(mapStateToProps, {
	getPlatforms,
	setPlatform
})(MenuPlatforms)