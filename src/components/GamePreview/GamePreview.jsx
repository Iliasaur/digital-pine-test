import React from 'react'
import * as gps from './game_preview_style'
import monthNames from '../../data/month_names'

const GamePreview = (props) => {

	let dt = new Date(props.game.released)

	return (
		<gps.Cover
			className="preview"
			to={"/game/" + props.game.slug}
		>
			<div>
				<gps.Poster className="preview__poster" style={{backgroundImage: "url(" + props.game.background_image + ")"}} />
				<gps.Title className="preview__title">{props.game.name}</gps.Title>
			</div>
			<gps.Stat className="preview__stat">
				<gps.Rating className="preview__rating" title="Rating">{props.game.rating}</gps.Rating>
				<div className="preview__date" title="Released">{monthNames[dt.getMonth()] + " " + dt.getDate() + ", " + dt.getFullYear()}</div>
			</gps.Stat>
		</gps.Cover>
	)
}



export default GamePreview
