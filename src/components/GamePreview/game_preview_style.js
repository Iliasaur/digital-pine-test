import {NavLink} from 'react-router-dom'
import styled from 'styled-components'
import device from '../../theme/device'


export const Cover = styled(NavLink)`
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	width: 100%;
	margin: 1rem 0;
	border-radius: ${(props) => props.theme.borderRadius.medium};
	overflow: hidden;
	color: ${(props) => props.theme.color.white};
	text-decoration: none;
	background-color: ${(props) => props.theme.color.dark};
	cursor: pointer;
	transition: background-color 0.25s;

	&:hover { background-color: ${(props) => props.theme.color.darkGray}; }

	@media ${device.tablet} { 
    width: calc(50% - 1.5rem);
  }

	@media ${device.laptop} { 
    width: calc(33% - 1.5rem);
  }

  @media ${device.desktop} { 
    width: calc(25% - 1.5rem);
  }
`

export const Poster = styled.div`
	height: calc(100vw * 0.55);
	background-size: cover;

	@media ${device.tablet} { 
    height: calc(50vw * 0.55);
  }

	@media ${device.laptop} { 
    height: calc(33vw * 0.55);
  }

  @media ${device.desktop} { 
    height: calc(25vw * 0.55);
  }
`

export const Title = styled.div`
	padding: 1rem;
	font-size: 1.3rem;
	font-weight: bold;
`

export const Stat = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	margin-top: 0.5rem;
	padding: 0 1rem 1rem 1rem;
	font-size: 0.9rem;
`

export const Rating = styled.div`
	background-color: ${(props) => props.theme.color.darkGray};
	padding: 0.3rem 0.7rem;
	border-radius: ${(props) => props.theme.borderRadius.small};
`