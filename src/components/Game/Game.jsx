import React from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
import {compose} from 'redux'

import {getGame, clearGame} from '../../redux/game-reducer'

import styled from 'styled-components'
import monthNames from '../../data/month_names'
import Preloader from '../ui/Preloader'
import Slider from '../ui/Slider'
import PageArt from '../ui/PageArt'



const Cover = styled.div`
	margin-top: 1rem;
`

const H1 = styled.div`
	font-size: 3rem;
	font-weight: bold;
	margin: 0 0 1rem 0;
`


export const Stat = styled.div`
	display: flex;
	align-items: center;
	margin: 1rem 0;
	padding: 0;
	font-size: 0.9rem;
`

export const StatItem = styled.div`
	display: flex;
	align-items: center;
	margin-right: 2rem;

	&:last-child {margin-right: 0;}
`

export const StatLabel = styled.label`
	margin-right: 0.5rem;
	color: ${props => props.theme.color.gray};
`

export const StatValue = styled.div`
	background-color: ${props => props.theme.color.darkGray};
	padding: 0.3rem 0.7rem;
	border-radius: ${props => props.theme.borderRadius.small};
`

export const Website = styled.p`
	color: ${props => props.theme.color.gray};

	a {
		color: ${props => props.theme.color.white};
		text-decoration: none;
		transition: color 0.25s;

		&:hover {color: ${props => props.theme.color.gray};}
	}
`

export const Description = styled.div`
	font-size: 1.2rem;
	line-height: 2rem;
	max-width: 960px;
`



class Game extends React.Component {

	//------------- Did Mount
	componentDidMount = () => {
		this.props.getGame(this.props.match.params.slug)
	}


	//------------- Will Unmount
	componentWillUnmount() {
		this.props.clearGame()
  }

	
	//------------- Render
	render = (props) => {
		let dt = new Date(this.props.game ? this.props.game.released : undefined)
		let description = []

		if (!this.props.isFetchingData && this.props.game) {
			if (this.props.game.description_raw) {
				description = this.props.game.description_raw.split("\n")	
			}

			if (!this.props.game.description_raw && this.props.game.description) {
				description = this.props.game.description.split("<br>").map(text => text.replace(/(<([^>]+)>)/gi, ""))
			}
		}

		

		return(
			<>
				<Cover>
					{
						this.props.isFetchingData || !this.props.game
							? <Preloader />
							: <>
									<H1>{this.props.game.name}</H1>
									<Stat className="game__stat">
										<StatItem className="game__rating">
											<StatLabel>Rating:</StatLabel><StatValue>{this.props.game.rating}</StatValue>
										</StatItem>
										<StatItem className="game__released">
											<StatLabel>Released:</StatLabel><StatValue>{monthNames[dt.getMonth()] + " " + dt.getDate() + ", " + dt.getFullYear()}</StatValue>
										</StatItem>
									</Stat>
									{
										this.props.game.website && <Website className="game__website">
											Website: <a href={this.props.game.website} rel="noreferrer" target="_blank">{this.props.game.website}</a>
										</Website>
									}

									{ (this.props.isFetchingScreenshots || !this.props.game) && <Preloader /> }
									{ !this.props.isFetchingScreenshots && this.props.game && (this.props.screenshots.length > 0) &&
											<Slider
												srcs={ this.props.screenshots.filter(screenshot => !screenshot.is_deleted) }
												autoplay={false}
												delay={3000}
											/>
									}

									<Description>
										{description.map((text, index) => <p key={index}>{text}</p>)
									}
									</Description>
								</>
					}
				</Cover>
				{!this.props.isFetchingData && this.props.game && this.props.game.background_image && <PageArt bg={this.props.game.background_image} />}
			</>
		)
	}
}



/*****************************************************************************

					CONTAINER

*****************************************************************************/

let mapStateToProps = (state) => {
	return {
		isFetchingData: state.game.isFetchingData,
		isFetchingScreenshots: state.game.isFetchingScreenshots,
		game: state.game.data,
		screenshots: state.game.screenshots
	}
}

export default compose(
	connect(mapStateToProps, {
		getGame,
		clearGame
	}),
	withRouter
)(Game)