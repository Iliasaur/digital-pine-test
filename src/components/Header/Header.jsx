import React from 'react'
import {Route} from 'react-router-dom'
import {connect} from 'react-redux'

import {clearGame} from '../../redux/game-reducer'
import {setKeyword} from '../../redux/games-reducer.js'

import styled from 'styled-components'
import device from '../../theme/device'
import Hamburger from '../ui/Hamburger'
import Searchbar from '../ui/Searchbar'


const Cover = styled.div`
	position: relative;
  display: flex;
  justify-content: space-between;
 	align-items: center;
 	padding: 0 0 1rem 0;

 	@media ${device.laptop} {
    justify-content: flex-start;
  }
`

const Title = styled.div`
  font-size: 1.8rem;

  @media ${device.laptop} {
    width: 250px;
    flex-shrink: 0;
  }
`

const SearchbarCover = styled.div`
	display: none;
	min-width: 200px;

	@media ${device.mobileL} {
		display: block;
  	width: calc(100% - 300px);
  }
`

const HamburgerCover = styled.div`
	@media ${device.laptop} {
    display: none;
  }
`


class Header extends React.Component {

	//------------- Set Keyword
	setKeyword = (keyword) => {
		this.props.clearGame()
		this.props.setKeyword(keyword)
	}


	//------------- Render
	render = () => {
		return (
			<Cover className="header">
				<Title className="header__title">GameStore</Title>
				<SearchbarCover className="searchbar__cover">
					<Route render={({history}) => (
						<Searchbar
							keyword={this.props.keyword}
							onChange={(keyword) => {
								this.setKeyword(keyword)
								history.push("/")
							}}
							maxLength={100}
							placeholder={this.props.gamesCount > 0 ? "Search " + (new Intl.NumberFormat().format(this.props.gamesCount)) + " games" : "Search games"}
						/>
					)} />
				</SearchbarCover>
				<HamburgerCover className="hamburger__cover">
					<Hamburger handleClick={this.props.toggleMenuVisibility} />
				</HamburgerCover>
			</Cover>
		)
	}

}


/*****************************************************************************

					CONTAINER

*****************************************************************************/

let mapStateToProps = (state) => {
	return {
		gamesCount: state.games.count,
		keyword: state.games.filter.keyword
	}
}

export default connect(mapStateToProps, {
	clearGame,
	setKeyword
})(Header)