import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  html, body {
    height: 100%;
    margin: 0;
  }

  html > body {
    min-height: 100%;
    height: auto;
    position: relative;
    margin: 0;
    padding: 0;
    color: ${props => props.theme.color.white};
    background-color: ${props => props.theme.color.body};
    font-family: 'RobotoRegular';
    z-index: -2;
  }

  a {
    color: ${props => props.theme.color.white};
    text-decoration: none;
    transition: color 0.25s;

    &:hover {
      color: ${props => props.theme.color.gray};
    }
  }
`

export default GlobalStyle