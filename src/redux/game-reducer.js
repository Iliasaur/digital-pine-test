
//------------- Imports

import api from '../api'




//------------- Constants

const GM_SET_GAME_SLUG = "GM_SET_GAME_SLUG"
const GM_SET_GAME_DATA = "GM_SET_GAME_DATA"
const GM_SET_GAME_SCREENSHOTS = "GM_SET_GAME_SCREENSHOTS"
const GM_TOGGLE_FETCHING_DATA = "GM_TOGGLE_FETCHING_DATA"
const GM_TOGGLE_FETCHING_SCREENSHOTS = "GM_TOGGLE_FETCHING_SCREENSHOTS"
const GM_CLEAR_GAME = "GM_CLEAR_GAME"


//------------- Initial State

let initialState = {
	slug: "",
	data: null,
	screenshots: [],
	isFetchingData: false,
	isFetchingScreenshots: false
}




//------------- Reducer

const gameReducer = (state = initialState, action) => {
	switch (action.type) {
		case GM_SET_GAME_SLUG: return {
			...state,
			slug: action.slug
		}

		case GM_SET_GAME_DATA: return {
			...state,
			data: {...action.data}
		}

		case GM_SET_GAME_SCREENSHOTS: return {
			...state,
			screenshots: [...action.screenshots]
		}

		case GM_TOGGLE_FETCHING_DATA: return {
			...state,
			isFetchingData: action.isFetching
		}

		case GM_TOGGLE_FETCHING_SCREENSHOTS: return {
			...state,
			isFetchingScreenshots: action.isFetching
		}

		case GM_CLEAR_GAME: return {
			...state,
			slug: "",
			data: null,
			screenshots: []
		}

		default: return state;
	}
}




//------------- Action Creators

const setGameSlug = (slug) => ({type: GM_SET_GAME_SLUG, slug})
const setGameData = (data) => ({type: GM_SET_GAME_DATA, data})
const setGameScreenshots = (screenshots) => ({type: GM_SET_GAME_SCREENSHOTS, screenshots})
const toggleFetchingData = (isFetching) => ({type: GM_TOGGLE_FETCHING_DATA, isFetching})
const toggleFetchingScreenshots = (isFetching) => ({type: GM_TOGGLE_FETCHING_SCREENSHOTS, isFetching})
export const clearGame = () => ({type: GM_CLEAR_GAME})




//------------- Thunks

export const getGame = (slug) => {
	return (dispatch) => {
		dispatch(setGameSlug(slug))

		dispatch(toggleFetchingData(true))
		api.getGame(slug).then(data => {
			dispatch(toggleFetchingData(false))
			dispatch(setGameData(data))
		})

		dispatch(toggleFetchingScreenshots(true))
		api.getGameScreenshots(slug).then(data => {
			dispatch(toggleFetchingScreenshots(false))
			dispatch(setGameScreenshots(data.results))
		})
	}
}



//------------- Export By Default

export default gameReducer