
//------------- Imports

import api from '../api'
import gameOrder from '../data/game_order'




//------------- Constants

const GMS_APPNED_GAMES_LIST = "GMS_APPNED_GAMES_LIST"
const GMS_SET_GAMES_COUNT = "GMS_SET_GAMES_COUNT"
const GMS_TOGGLE_FETCHING = "GMS_TOGGLE_FETCHING"
const GMS_SET_FILTER = "GMS_SET_FILTER"
const GMS_SET_ORDER = "GMS_SET_ORDER"
const GMS_CLEAR_GAMES_LIST = "GMS_CLEAR_GAMES_LIST"




//------------- Initial State

let initialOrder = gameOrder.length > 0 ? gameOrder[0] : {title: "Relevance", key: "", isDescByDefault: false}

let initialState = {
	list: [],
	count: 0,
	filter: {
		page: 1,
		pageSize: 20,
		platforms: [],
		keyword: ""
	},
	order: {
		key: initialOrder.key,
		isDesc: initialOrder.isDescByDefault
	},
	isFetching: false
}





//------------- Reducer

const gamesReducer = (state = initialState, action) => {
	switch (action.type) {
		case GMS_APPNED_GAMES_LIST: return {
			...state,
			list: [...state.list, ...action.gamesList]
		}

		case GMS_SET_GAMES_COUNT: return {
			...state,
			count: action.gamesCount
		}

		case GMS_TOGGLE_FETCHING: return {
			...state,
			isFetching: action.isFetching
		}

		case GMS_SET_FILTER: return {
			...state,
			filter: {...action.filter}
		}

		case GMS_SET_ORDER: return {
			...state,
			order: {...action.order}
		}

		case GMS_CLEAR_GAMES_LIST: return {
			...state,
			list: [],
			count: 0,
			filter: {...state.filter, page: 1}
		}

		default: return state;
	}
}






//------------- Action Creators

const appendGamesList = (gamesList) => ({type: GMS_APPNED_GAMES_LIST, gamesList})
const setGamesCount = (gamesCount) => ({type: GMS_SET_GAMES_COUNT, gamesCount})
const toggleFetching = (isFetching) => ({type: GMS_TOGGLE_FETCHING, isFetching})
const setFilter = (filter) => ({type: GMS_SET_FILTER, filter})
const setOrder = (order) => ({type: GMS_SET_ORDER, order})
const clearGamesList = () => ({type: GMS_CLEAR_GAMES_LIST})




//------------- Thunks

export const getGames = () => {
	return (dispatch, getState) => {
		let state = getState().games
		if (state.isFetching) { return }
		
		let pagesCount = Math.ceil(state.count / state.filter.pageSize)
		let page = pagesCount > 0 ? (state.filter.page < pagesCount ? state.filter.page + 1 : state.filter.page) : 1
		let filter = {...state.filter, page}

		dispatch(toggleFetching(true))

		dispatch(setFilter(filter))
		api.getGames(filter, state.order).then(data => {
			dispatch(toggleFetching(false))
			dispatch(setGamesCount(data.count))
			dispatch(appendGamesList(data.results))
		})
	}
}



export const setPlatform = (platform) => {
	return (dispatch, getState) => {
		dispatch(clearGamesList())
		dispatch(toggleFetching(true))

		let state = getState().games
		let filter = {...state.filter, platforms: isNaN(platform) || Number(platform) <= 0 ? [] : [platform]}

		dispatch(setFilter(filter))
		api.getGames(filter, state.order).then(data => {
			dispatch(toggleFetching(false))
			dispatch(setGamesCount(data.count))
			dispatch(appendGamesList(data.results))
		})
	}
}



export const setKeyword = (keyword) => {
	return (dispatch, getState) => {
		dispatch(clearGamesList())
		dispatch(toggleFetching(true))

		let state = getState().games
		let filter = {...state.filter, keyword}

		dispatch(setFilter(filter))
		api.getGames(filter, state.order).then(data => {
			dispatch(toggleFetching(false))
			dispatch(setGamesCount(data.count))
			dispatch(appendGamesList(data.results))
		})
	}
}



export const setOrderKey = (key, isDesc) => {
	return (dispatch, getState) => {
		let masterOrder = gameOrder.find(order => order.key === key);
		if (!masterOrder) { return }

		let state = getState().games
		if (state.order.key === key) { return }

		dispatch(clearGamesList())
		dispatch(toggleFetching(true))
		
		let order = {key, isDesc: masterOrder.isDescByDefault}

		dispatch(setOrder(order))
		api.getGames(state.filter, order).then(data => {
			dispatch(toggleFetching(false))
			dispatch(setGamesCount(data.count))
			dispatch(appendGamesList(data.results))
		})
	}
}



export const toggleOrderDirection = () => {
	return (dispatch, getState) => {
		dispatch(clearGamesList())
		dispatch(toggleFetching(true))

		let state = getState().games		
		let order = {...state.order, isDesc: !state.order.isDesc}

		dispatch(setOrder(order))
		api.getGames(state.filter, order).then(data => {
			dispatch(toggleFetching(false))
			dispatch(setGamesCount(data.count))
			dispatch(appendGamesList(data.results))
		})
	}
}



//------------- Export By Default

export default gamesReducer
