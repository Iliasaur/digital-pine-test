
//------------- Imports

import api from '../api'





//------------- Constants

const PTF_SET_PLATFORMS_LIST = "PTF_SET_PLATFORMS_LIST"
const PTF_TOGGLE_FETCHING = "PTF_TOGGLE_FETCHING"






//------------- Initial State

let initialState = {
	list: [],
	count: 0,
	isFetching: false
}





//------------- Reducer

const platformsReducer = (state = initialState, action) => {
	switch (action.type) {
		case PTF_SET_PLATFORMS_LIST: return {
			...state,
			list: [...action.platformsList]
		}

		case PTF_TOGGLE_FETCHING: return {
			...state,
			isFetching: action.isFetching
		}

		default: return state;
	}
}






//------------- Action Creators

const setPlatformsList = (platformsList) => ({type: PTF_SET_PLATFORMS_LIST, platformsList})
const toggleFetching = (isFetching) => ({type: PTF_TOGGLE_FETCHING, isFetching})





//------------- Thunks

export const getPlatforms = () => {
	return (dispatch) => {
		dispatch(toggleFetching(true))
		api.getPlatforms().then(data => {
			dispatch(toggleFetching(false))
			dispatch(setPlatformsList(data.results))
		})
	}
}






//------------- Export By Default

export default platformsReducer
