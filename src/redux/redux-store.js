import {applyMiddleware, createStore, combineReducers} from 'redux'
import gamesReducer from './games-reducer.js'
import gameReducer from './game-reducer.js'
import platformsReducer from './platforms-reducer.js'
import thunkMiddleware from 'redux-thunk'


let reducers = combineReducers({
	games: gamesReducer,
	game: gameReducer,
	platforms: platformsReducer
})

let store = createStore(reducers, applyMiddleware(thunkMiddleware))

export default store