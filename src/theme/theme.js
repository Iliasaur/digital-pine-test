const theme = {
  color: {
    black: '#000000',
    dark: '#202020',
    white: '#ffffff',
    light: '#e8e8e8',
    gray: '#888888',
    darkGray: '#3b3b3b',
    lightGray: '#c8c8c8',
    body: '#161616',
    blue: '#1779ba'
  },
  borderRadius: {
    small: '0.3rem',
    medium: '0.5rem',
    large: '0.7rem',
    huge: '2rem'
  }
}

export default theme