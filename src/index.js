import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { Provider } from 'react-redux'
import theme from './theme'
import App from './App'
import store from './redux/redux-store.js'


ReactDOM.render(
  <React.StrictMode>
	  <BrowserRouter>
			<Provider store={store}>
				<ThemeProvider theme={theme}>
    			<App />
    		</ThemeProvider>
    	</Provider>
		</BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);
