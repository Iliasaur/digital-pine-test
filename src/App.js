import React from 'react'
import {Route} from 'react-router-dom'

import GlobalStyle from './GlobalStyle'
import GlobalFonts from './fonts/fonts'
import styled from 'styled-components'
import device from './theme/device'

import Header from './components/Header'
import Footer from './components/Footer'
import Menu from './components/Menu'
import GameList from './components/GameList'
import Game from './components/Game'



const AppHolder = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  max-width: 600px;
  padding: 1rem;

  @media ${device.tablet} { 
    max-width: 100%;  
  }

   @media ${device.desktopL} { 
    max-width: 1980px;  
  }
`

const Body = styled.div`
  @media ${device.laptop} { 
    display: flex;
  }
`


const Sidebar = styled.div`
  @media ${device.laptop} { 
    position: relative;
    width: 250px;
    flex-shrink: 0;
  }
`



class App extends React.Component {

  state = {
    isMenuVisible: false,
    isScrolled: false
  }


  //------------- Did Mount
  componentDidMount = () => {
    window.addEventListener("scroll", this.handleScroll)
  }


  //------------- Will Unmount
  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll)
  }


  //------------- Handle Scroll
  handleScroll = () => {
    this.setState({...this.state, isScrolled: window.pageYOffset > 100})
  }


  //------------- Toggle Menu Visibility
  toggleMenuVisibility = () => {
    this.setState({...this.state, isMenuVisible: !this.state.isMenuVisible})
  }


  //------------- Render
  render = () => {
    return (
      <>
        <GlobalFonts />
        <GlobalStyle />
        <AppHolder className="app_hoder">
          <Header toggleMenuVisibility={this.toggleMenuVisibility} />
          <Body className="app_body">
            <Sidebar>
              <Menu isMenuVisible={this.state.isMenuVisible} isScrolled={this.state.isScrolled} toggleMenuVisibility={this.toggleMenuVisibility} />
            </Sidebar>
            <Route
              exact
              path="/"
              component={GameList}
            />
            <Route
              path="/game/:slug"
              component={Game}
            />
          </Body>
          { this.state.isScrolled && <Footer toggleMenuVisibility={this.toggleMenuVisibility} /> }
        </AppHolder>

      </>
    )
  }
}

export default App