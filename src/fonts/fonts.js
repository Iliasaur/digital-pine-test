import { createGlobalStyle } from 'styled-components';

import RobotoRegular from './roboto/RobotoRegular/RobotoRegular.woff';

export default createGlobalStyle`
    @font-face {
        font-family: 'RobotoRegular';
        src: local('RobotoRegular'),
        url(${RobotoRegular}) format('woff');
        font-weight: 300;
        font-style: normal;
    }
`

